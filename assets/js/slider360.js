var Slider360 = function(params) {

    this.ready = false,
        this.dragging = false,
        this.pointerStartPosX = 0,
        this.pointerEndPosX = 0,
        this.pointerDistance = 0,
        this.monitorStartTime = 0,
        this.monitorInt = 200,
        this.ticker = 0,
        this.currentFrame = 0,

        this.speedMultiplier = params.speedMultiplier || 1,
        this.totalFrames = params.totalFrames,
        this.imageName = params.imageName || "slider-image",
        this.imageExtension = params.imageExtension || ".png",
        this.folderName = params.folderName || "assets/img/",

        this.frames = [],
        this.endFrame = 0,
        this.loadedImages = 0,
        this.container = document.getElementById(params.container || "slider360"),
        this.images = document.getElementById(params.images || "slider-image"),
        this.loader = document.getElementById(params.loader || "acf-loader"),

        this.demoMode = params.demoMode,
        this.launchAnimTime = params.launchAnimTime || 0,
        this.speedDemo = params.speedDemo * 100 || 250,
        this.fakePointer = {
            x: 8,
            speed: 1
        }
}

Slider360.prototype.addSpinner = function() {
    this.container.appendChild(this.loader);
    this.loader.classList.remove('motion', 'fade-out');
    this.loader.classList.add('motion', 'fade-in');
    this.loader.style.display = "block";
};

Slider360.prototype.loadImage = function() {
    var li = document.createElement("li"),
        imageName = this.folderName + this.imageName + (this.loadedImages + 1) + this.imageExtension,
        image = document.createElement("img"),
        that = this;

    image.src = imageName;
    image.classList.add("previous-image");

    li.appendChild(image);

    this.frames.push(image);

    this.images.appendChild(li);

    image.addEventListener("load", function() {
        that.imageLoaded();
    })
};

Slider360.prototype.imageLoaded = function() {
    this.loadedImages++;
    console.log("loadedImages", this.loadedImages);

    if (this.loadedImages == this.totalFrames) {
        this.frames[0].classList.remove("previous-image");
        this.frames[0].classList.add("current-image");

        this.showSlider();

        this.loader.classList.remove('motion', 'fade-in');
        this.loader.classList.add('motion', 'fade-out');

        this.initMove();
    } else {
        this.addSpinner();
        this.loadImage();
    }
};

Slider360.prototype.showSlider = function() {
    var self = this;

    this.endFrame = 0;

    this.ready = true;

    if (!this.demoMode) {
        this.refresh();
    } else {
        this.fakePointerTimer = setInterval(function() {
            self.moveFakePointer();
        }, this.speedDemo);
        console.log(this.speedDemo);

    }
};

Slider360.prototype.moveFakePointer = function()  {
    var self = this;

    this.fakePointer.x += this.fakePointer.speed;

    var launch = setTimeout(function() {
        self.trackPointer();
    }, this.launchAnimTime);
};

Slider360.prototype.quitDemoMode = function()  {
    window.clearInterval(this.fakePointerTimer);
    this.demoMode = false;
};


Slider360.prototype.renderAnim = function() {
    if (this.currentFrame !== this.endFrame) {
        var frameEasing = this.endFrame < this.currentFrame ? Math.floor((this.endFrame - this.currentFrame) * 0.1) : Math.ceil((this.endFrame - this.currentFrame) * 0.1);
        this.hidePreviousFrame();
        this.currentFrame += frameEasing;
        this.showCurrentFrame();
    } else {
        clearInterval(this.ticker);
        this.ticker = 0;
    }
};

Slider360.prototype.refresh = function() {
    var that = this;
    if (this.ticker === 0) {
        this.ticker = setInterval(function() {
            that.renderAnim();
        }, Math.round(1000 / 60));
    };
};

Slider360.prototype.hidePreviousFrame = function() {
    var frame = this.frames[this.getNormalizedCurrentFrame()];
    var removeClass = frame.classList.remove('current-image');
    var addClass = frame.classList.add('previous-image');
};

Slider360.prototype.showCurrentFrame = function() {
    var frame = this.frames[this.getNormalizedCurrentFrame()];
    var removeClass = frame.classList.remove('previous-image');
    var addClass = frame.classList.add('current-image');
};

Slider360.prototype.getNormalizedCurrentFrame = function() {
    var c = Math.ceil(this.currentFrame % this.totalFrames);
    if (c < 0) c += (this.totalFrames);
    return c;
};

Slider360.prototype.getPointerEvent = function(event) {
    return event;
};

Slider360.prototype.trackPointer = function(event, o) {
    var userDragging = this.ready && this.dragging ? true : false;
    var demoDragging = this.demoMode;
    var pointerDistance = o ? o.dx : this.pointerDistance;

    if ((userDragging && o) || demoDragging) {

        this.pointerEndPosX = userDragging ? (o.dx) : this.fakePointer.x;

        if (this.monitorStartTime < new Date().getTime() - this.monitorInt) {

            pointerDistance = this.pointerEndPosX - this.pointerStartPosX;

            if (userDragging) {
                this.endFrame += o.direction == "left" ? -1 : 1;
            } else {
                this.endFrame = this.currentFrame + Math.ceil((this.totalFrames - 1) * (pointerDistance / window.innerWidth) / 3);
            }

            this.refresh();

            this.monitorStartTime = new Date().getTime();

            this.pointerStartPosX = userDragging ? o.dx : this.fakePointer.x;
        }
    } else {
        return;
    }
};

Slider360.prototype.move = function(event, o) {
    event.preventDefault();
    if (o.inMotion) {
        this.quitDemoMode();
        this.pointerStartPosX = o.start.left;
        this.dragging = true;
        this.trackPointer(event, o);
    } else {
        this.dragging = false;
    }
}

Slider360.prototype.initMove = function() {
    this.on(window, 'slide', this.move);
}

Slider360.prototype.on = function(element, evt, callback) {
    var that = this;

    var isSlide = evt == 'slide',
        detecttouch = !!('ontouchstart' in window) || !!('ontouchstart' in document.documentElement) || !!window.ontouchstart || !!window.onmsgesturechange || (window.DocumentTouch && window.document instanceof window.DocumentTouch);
    if (isSlide) {
        var o = {},
            evtStarted = false,
            evtStart = function(e) {
                var evt = e.changedTouches ? e.changedTouches[0] : e;
                evtStarted = true;
                o = {
                    start: {
                        left: evt.pageX,
                        top: evt.pageY
                    }
                };
            },
            evtEnd = function(e) {
                if (!evtStarted) return;
                var evt = e.changedTouches ? e.changedTouches[0] : e;
                o.end = {
                    left: evt.pageX,
                    top: evt.pageY
                };
                o.dx = o.end.left - o.start.left;
                o.dy = o.end.top - o.start.top;
                o.angle = Math.atan2(o.dy, o.dx);
                o.angle *= 180 / Math.PI;
                o.inMotion = (e.type == 'touchmove' || e.type == 'mousemove');
                o.direction = Math.abs(o.dx) > Math.abs(o.dy) ? ('' + o.dx).indexOf('-') != -1 ? 'left' : 'right' : ('' + o.dy).indexOf('-') != -1 ? 'top' : 'bottom',
                    callback.apply(that, [e, o]);
                if (o.inMotion == false) evtStarted = false;
            };
        if (detecttouch) {
            element.addEventListener('touchstart', evtStart, false);
            element.addEventListener('touchmove', evtEnd, false);
            element.addEventListener('touchend', evtEnd, false);
        } else {
            element.addEventListener('mousedown', evtStart, false);
            element.addEventListener('mousemove', evtEnd, false);
            element.addEventListener('mouseup', evtEnd, false);
        }
    }
};
